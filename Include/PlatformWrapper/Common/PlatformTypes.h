//  -*-  coding: utf-8; mode: c++  -*-  //
/*************************************************************************
**                                                                      **
**                  ---  Platform Wrapper Library  ---                  **
**                                                                      **
**                                                                      **
**          Copyright (C), 2016-2016, Takahiro Itou                     **
**          All Rights Reserved.                                        **
**                                                                      **
*************************************************************************/

/**
**      Type Definitions.
**
**      @file       Common/PlatformTypes.h
**/

#if !defined( PFWRAPPER_COMMON_INCLUDED_PLATFORM_TYPES_H )
#    define   PFWRAPPER_COMMON_INCLUDED_PLATFORM_TYPES_H

#include    "PlatformSettings.h"

PLATFORM_WRAPPER_NAMESPACE_BEGIN

//========================================================================
//
//    Type Definitions.
//

PLATFORM_WRAPPER_NAMESPACE_END

#endif
