//  -*-  coding: utf-8; mode: c++  -*-  //
/*************************************************************************
**                                                                      **
**                  ---  Platform Wrapper Library  ---                  **
**                                                                      **
**                                                                      **
**          Copyright (C), 2016-2016, Takahiro Itou                     **
**          All Rights Reserved.                                        **
**                                                                      **
*************************************************************************/

/**
**      An Implementation of Test Case 'PlatformSettings'.
**
**      @file       Common/Tests/PlatformSettingsTest.cpp
**/

#include    "TestDriver.h"

#include    "PlatformWrapper/Common/PlatformSettings.h"

PLATFORM_WRAPPER_NAMESPACE_BEGIN

//========================================================================
//
//    PlatformSettingsTest  class.
//
/**
**    ファイル PlatformSettings.h の単体テスト。
**/

class  PlatformSettingsTest : public  TestFixture
{
    CPPUNIT_TEST_SUITE(PlatformSettingsTest);
    CPPUNIT_TEST_SUITE_END();

public:
    virtual  void   setUp()     OVERRIDE    { }
    virtual  void   tearDown()  OVERRIDE    { }

private:

};

CPPUNIT_TEST_SUITE_REGISTRATION( PlatformSettingsTest );

//========================================================================
//
//    Tests.
//

PLATFORM_WRAPPER_NAMESPACE_END

//========================================================================
//
//    エントリポイント。
//

int  main(int argc, char * argv[])
{
    return ( executeCppUnitTests(argc, argv) );
}
